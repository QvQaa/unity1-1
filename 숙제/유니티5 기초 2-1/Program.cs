﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 유니티5_기초_2_1
{
    public enum Country {Korea, China, Japan}

    public struct Employee
    {
        public int BirthYear;
        public string Name;
    }

    class Program
    {

        public class Product
        {
            public const int ConstPrice = 1000;
            public readonly int ReadOnlyPrice;

            public Product()
            {
                this.ReadOnlyPrice = 2000;
            }

            public Product(int price)
            {
                this.ReadOnlyPrice = price;
            }
        }

        static void Main(string[] args)
        {
            //  Console.WriteLine("Hello World!!");

            //-----------------------------------------------------------------

            /*
            Country location1=Country Korea;
             Country location2 = (Country)1;
             int location3 = (int)location1;

             Console.WriteLine("location1 {0}",location1);
             Console.WriteLine("location2 {0}",location2);
             Console.WriteLine("location3(int) {0}",location3);
             Console.WriteLine("location3(Country) {0}",(Country)location3);
             */

            //---------------------------------------------------------------

            /*
            int? number = null;
            number = 10;

            if (number.HasValue)
            {
                Console.WriteLine(number.Value);
            }
            else
            {
                Console.WriteLine("값이 Null입니다");
            }
            */

            //---------------------------------------------------------------
            /*
            int? number = null;

            int defaultNumber1 = (number == null) ? 0 : (int)number;
            Console.WriteLine("defaultNumber1 : {0}", defaultNumber1);

            int defaultNumber2 = number ?? 0;
            Console.WriteLine("defaultNumber2 : {0}", defaultNumber2);
            */

            //----------------------------------------------------------------

            /*
            Employee emp1 = new Employee();
            emp1.Name = "이상원";
            emp1.BirthYear = 1994;

            Employee emp2 = emp1;
            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);

            emp1.BirthYear = 1998;
            Console.WriteLine("===emp1.BirthYear=1998 값 변경===");
            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);
            */

            //----------------------------------------------------------------

            /*
            string value1 = "값1";
            var value2 = "값2";
            dynamic value3 = "값3";

            Console.WriteLine("value1 : {0}", value1);
            Console.WriteLine("value2 : {0}", value2);
            Console.WriteLine("value3 : {0}", value3);
            */

            //---------------------------------------------------------------

            /*
            Console.WriteLine("ConstPrice={0}", Product.ConstPrice);

            Product item1 = new Product();
            Console.WriteLine("new Product() : ReadOnlyPrice={0}", item1.ReadOnlyPrice);

            Product item2 = new Product(3000);
            Console.WriteLine("new Product(3000) : ReadOnlyPrice={0}", item2.ReadOnlyPrice);
            */

            //----------------------------------------------------------------

            String numberString = "123";

            int number1 = Convert.ToInt32(numberString);

            //int number2=Int32.Parse(numberString);
            int number2 = int.Parse(numberString);

            Console.WriteLine("number1 : {0}", number1);
            Console.WriteLine("number2 : {0}", number2);


        }
    }
}
